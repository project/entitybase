<?php

/**
 * @file
 * Contains \Drupal\entity_base\Controller\EntityBaseSimpleController.
 */

namespace Drupal\entity_base\Controller;

/**
 * Controller routines for entity routes.
 */
class EntityBaseSimpleController extends EntityBaseBasicController {

}
