<?php

/**
 * @file
 * Contains \Drupal\entity_base\EntityBaseGenericViewBuilder.
 */

namespace Drupal\entity_base;

/**
 * Render controller for entities.
 */
class EntityBaseGenericViewBuilder extends EntityBaseViewBuilder {

}
