<?php

namespace Drupal\entity_base\Exception;

use RuntimeException;

/**
 * Thrown when entity can not obtain a lock.
 */
class LockException extends RuntimeException {

}
